#
#  Be sure to run `pod spec lint MyExtensions.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "MyExtensions"
  spec.version      = "0.0.1"
  spec.summary      = "My Cool Extensions"
  spec.description  = "My Cool Extensions that I can reuse in diffrent projects."
  spec.homepage     = "https://gitlab.com/DivyaNissi/myextensions"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "Divya Kothagattu" => "divya.kothagattu2011@gmail.com"}
  spec.platform     = :ios, "9.0"
  spec.source       = { :git => "git@gitlab.com:DivyaNissi/myextensions.git", :tag => "#{spec.version}" }
  spec.source_files = "MyExtensions/MyExtensions/**/*.{swift}"
  spec.exclude_files = "Classes/Exclude"

end
